<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    // Create Data
    public function create()
    {
        return view('post.create');
    }

    public function store(Request $request)
    {
        $request->validate([
         'title' => 'required|unique:posts',
         'body' => 'required',
     ]);
        $query = DB::table('posts')->insert([
         "title" => $request["title"],
         "body" => $request["body"]
     ]);
        return redirect('/posts');
    }

    // Show Data
    public function index()
    {
        $post = DB::table('posts')->get();
        return view('post.index', compact('post'));
    }

    public function show($id)
    {
        $post = DB::table('posts')->where('id', $id)->first();
        return view('post.show', compact('post'));
    }

    // Update Data
    public function edit($id)
    {
        $post = DB::table('posts')->where('id', $id)->first();
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
        'title' => 'required|unique:posts',
        'body' => 'required',
    ]);

        $query = DB::table('posts')
        ->where('id', $id)
        ->update([
            'title' => $request["title"],
            'body' => $request["body"]
        ]);
        return redirect('/posts');
    }

    // Delete Data
    public function destroy($id)
    {
        $query = DB::table('posts')->where('id', $id)->delete();
        return redirect('/posts');
    }
}
