<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActorController extends Controller
{
    // Create Data
    public function create()
    {
        return view('actor.create');
    }

    public function store(Request $request)
    {
        $request->validate([
         'nama' => 'required',
         'umur' => 'required',
         'bio' => 'required',
     ]);
        $query = DB::table('actors')->insert([
         "nama" => $request["nama"],
         "umur" => $request["umur"],
         "bio" => $request["bio"]
     ]);
        return redirect('/actors');
    }

    // Show Data
    public function index()
    {
        $actor = DB::table('actors')->get();
        return view('actor.index', compact('actor'));
    }

    public function show($id)
    {
        $actor = DB::table('actors')->where('id', $id)->first();
        return view('actor.show', compact('actor'));
    }

    // Update Data
    public function edit($id)
    {
        $actor = DB::table('actors')->where('id', $id)->first();
        return view('actor.edit', compact('actor'));
    }

    public function update($id, Request $request) {
        $request->validate([
        'nama' => 'required',
        'umur' => 'required',
        'bio' => 'required',
    ]);

        $query = DB::table('actors')
        ->where('id', $id)
        ->update([
            'nama' => $request["nama"],
            'umur' => $request["umur"],
            'bio' => $request["bio"]
        ]);
        return redirect('/actors');
    }

    // Delete Data
    public function destroy($id)
    {
        $query = DB::table('actors')->where('id', $id)->delete();
        return redirect('/actors');
    }
}
