<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

// Day 1
Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@submit');

// Day 2
Route::get('/admin', function() {
    return view('admin.index');
});

Route::get('/table', function() {
    return view('admin.table');
});

Route::get('/data-table', function() {
    return view('admin.data-table');
});

// Day 5 Belajar CRUD
Route::get('/actors', 'ActorController@index');
Route::get('/actors/create', 'ActorController@create');
Route::post('/actors', 'ActorController@store');
Route::get('actors/{actor_id}', 'ActorController@show');
Route::get('actors/{actor_id}/edit', 'ActorController@edit');
Route::put('actors/{actor_id}', 'ActorController@update');
Route::delete('actors/{actor_id}', 'ActorController@destroy');
