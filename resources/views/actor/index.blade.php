@extends('layout.actor')

@section('judul')
    All Data Actors
@endsection

@section('content')
<a class="btn btn-success float-right" href="/actors/create"><i class="fa fa-plus mr-2"></i>Tambah Data</a>
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Bio</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($actor as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td>
                <td>
                    <a href="/actors/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/actors/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="/actors/{{$value->id}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5" class="text-center font-italic">No data</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection
