@extends('layout.actor')

@section('judul')
    Edit Data Actor {{$actor->id}}
@endsection

@section('content')

<div>
    <form action="/actors/{{$actor->id}}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">Name</label>
            <input type="text" class="form-control" name="nama" value="{{$actor->nama}}" id="nama" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Age</label>
            <input type="text" class="form-control" name="umur"  value="{{$actor->umur}}" id="umur" placeholder="Masukkan umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" name="bio"  value="{{$actor->bio}}"  id="bio" placeholder="Masukkan bio">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>

@endsection
