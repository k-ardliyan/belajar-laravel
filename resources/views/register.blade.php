<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <div>
            <label for="firstname">First Name:</label> <br><br>
            <input type="text" id="firstname" name="firstname"> <br><br>
        </div>
        <div>
            <label for="lastname">Last Name:</label><br><br>
            <input type="text" id="lastname" name="lastname"><br><br>
        </div>
        <div>
            <label for="gender">Gender:</label><br><br>
            <input type="radio" name="male" id="gender" value="male">Male<br>
            <input type="radio" name="female" id="gender" value="female">Female<br>
            <input type="radio" name="other" id="gender" value="other">Other<br><br>
        </div>
        <div>
            <label for="nationality">Nationality:</label><br><br>
            <select name="nationality" id="nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="Singaporan">Singaporan</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Australian">Australian</option>
            </select><br><br>
        </div>
        <div>
            <label for="language-spoken">Language Spoken:</label><br><br>
            <input type="checkbox" name="bahasa-indonesia" id="language-spoken">Bahasa Indonesia<br>
            <input type="checkbox" name="english" id="language-spoken">English<br>
            <input type="checkbox" name="other" id="language-spoken">Other<br><br>
        </div>
        <div>
            <label for="bio">Bio:</label><br><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        </div>
        <button type="submit">Sign Up</button>
    </form>
</body>
</html>
