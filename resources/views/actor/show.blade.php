@extends('layout.actor')

@section('judul')
    Detail Data Actor {{$actor->id}}
@endsection

@section('content')

<h4>Nama: {{$actor->nama}}</h4>
<p>Umur: {{$actor->umur}}</p>
<p>Biografi: {{$actor->bio}}</p>

@endsection
